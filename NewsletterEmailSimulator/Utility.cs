﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NewsletterEmailSimulator
{
    static class Utility
    {
        public static void LogExceptionToFile(Exception Ex)
        {
            try
            {
                if (!Directory.Exists(Config.LogDirectory))
                {
                    Directory.CreateDirectory(Config.LogDirectory);
                }

                using (StreamWriter swr = new StreamWriter(
                    $@"{Config.LogDirectory}\{Config.LogFilePrefix}{DateTime.Today:MM-dd-yyy}.txt", true))
                {
                    swr.WriteLine($"Exception occured at: {DateTime.Now}");
                    swr.WriteLine(Ex.Message);
                    swr.WriteLine(Ex.StackTrace);
                    swr.WriteLine("------------------------------------------------------------------------");
                    swr.Close();
                }
            }
            catch (Exception Exp)
            {
                Console.WriteLine(Exp.Message);
                Console.WriteLine(Exp.StackTrace);
            }
        }
        public static void LogMessageToFile(string message)
        {
            try
            {
                if (!Directory.Exists(Config.LogDirectory))
                {
                    Directory.CreateDirectory(Config.LogDirectory);
                }

                using (StreamWriter swr = new StreamWriter(
                    $@"{Config.LogDirectory}\{Config.LogFilePrefix}{DateTime.Today:MM-dd-yyy}.txt", true))
                {
                    swr.WriteLine($"Message logged at: {DateTime.Now}");
                    swr.WriteLine(message);
                    swr.WriteLine("------------------------------------------------------------------------");
                    swr.Close();
                }
            }
            catch (Exception Exp)
            {
                Console.WriteLine(Exp.Message);
                Console.WriteLine(Exp.StackTrace);
            }
        }
        public static void SendEmail(string subject, string bodyContent, string ToEmail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient(Config.SMTPServer);

                mail.From = new MailAddress(Config.FromEmail);

                if (ToEmail.Contains(';'))
                {
                    foreach (var item in ToEmail.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            mail.To.Add(item);
                        }
                    }
                }
                else { mail.To.Add(ToEmail); }

                mail.Subject = Config.ExecutionEnvironment + "-" + subject;
                mail.Body = bodyContent.Replace("\r\n", "<br/>");
                mail.IsBodyHtml = true;
                smtpServer.Port = int.Parse(Config.SMTPPort);
                smtpServer.Credentials = new NetworkCredential(Config.SMTPUsername, Config.SMTPPassword);
                smtpServer.EnableSsl = bool.Parse(Config.EnableSSL);
                smtpServer.Timeout = int.Parse(Config.SMTPTimeOut) * 1000;
                smtpServer.Send(mail);
            }
            catch (Exception exp)
            {
                LogMessageToFile(exp.Message);
                LogMessageToFile(exp.StackTrace);
            }
        }
        public static Int64 DateTimeToUnixTimeStamp(DateTime dtLastRun)
        {
           // DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);
           // TimeSpan span = (dtLastRun - epoch);
            var ms = (DateTime.Now - DateTime.MinValue).TotalMilliseconds;
            System.Diagnostics.Debug.WriteLine("Milliseconds since the alleged birth of christ: " + ms);
            return (Int64)ms;
        }
        public static void SendEmail(string subject, string bodyContent)
        {
            SendEmail(subject, bodyContent, Config.ToEmail);
        }
        public static DateTime TokenIssuedDateTime { get; set; }
        public static string GetAuthTokenFromFile()
        {
            if (!File.Exists($@"{Config.LogDirectory}\{"token.txt"}"))
            {
                TokenIssuedDateTime = DateTime.MinValue;
                return string.Empty;
            }

            using (StreamReader swr = new StreamReader($@"{Config.LogDirectory}\{"token.txt"}"))
            {
                string tdateTime = swr.ReadLine();
                DateTime tokenDateTime = DateTime.Parse(tdateTime);
                TokenIssuedDateTime = tokenDateTime;
                string jsonData = swr.ReadLine();
                JavaScriptSerializer js = new JavaScriptSerializer();
                AccessTokenResponse accessTokenResponse = js.Deserialize<AccessTokenResponse>(jsonData);

                if (tokenDateTime.AddSeconds(accessTokenResponse.Expires_In + 150) > DateTime.Now)
                {
                    return jsonData;
                }
                else
                {
                    TokenIssuedDateTime = DateTime.MinValue;
                    return string.Empty;
                }
            }
        }
        public static void SaveAuthToken(string authToken)
        {
            using (StreamWriter swr = new StreamWriter($@"{Config.LogDirectory}\{"token.txt"}"))
            {
                swr.WriteLine(DateTime.Now);
                swr.WriteLine(authToken);
                swr.Close();
            }
        }
    }
}
