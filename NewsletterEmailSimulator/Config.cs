﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsletterEmailSimulator
{
    public static class Config
    {
        private static string _appGroupName;
        private static BHI.Configuration.Config _bhiConfig;
        public static string AppGroupName
        {
            get { return _appGroupName ?? "BDXConnect:Newsletter"; }
            set { _appGroupName = value; }
        }
        private static string Get<T>(string key)
        {
            _bhiConfig = _bhiConfig ?? new BHI.Configuration.Config();
            return _bhiConfig.GetValue(AppGroupName, key).ToString();
        }
        public static string BaseUrl => Get<string>("BaseUrl");
        public static string LogDirectory => Get<string>("LogDirectory") + "ProTestLog\\";
        public static string LogFilePrefix => Get<string>("LogFilePrefix");
        public static string SMTPServer => Get<string>("SMTPServer");
        public static string SMTPPort => Get<string>("SMTPPort");
        public static string SMTPUsername => Get<string>("SMTPUsername");
        public static string SMTPPassword => Get<string>("SMTPPassword");
        public static string FromEmail => Get<string>("FromEmail");
        public static string SMTPTimeOut => Get<string>("SMTPTimeOut");

        public static string ToEmail => Get<string>("ToEmail");

        public static string AllowedAccounts(int brandPartnerId)
        {
            return Get<string>("AllowedAccounts_$88");
        }
        public static string GetEmailFromAddress(int brandPartnerId)
        {
            return Get<string>($"EmailFromAddress_${brandPartnerId}");
        }
        public static string GetEmailSubject(int brandPartnerId)
        {
            return Get<string>($"EmailSubject_${brandPartnerId}");
        }
        public static string EmailTemplateUrl(int brandPartnerId)
        {
            return Get<string>($"EmailTemplateUrl_${brandPartnerId}");
        }
        public static string EmailCategory(int brandPartnerId)
        {
            return Get<string>($"EmailCategory_${brandPartnerId}");
        }

        public static string GetWebsiteDomain(int brandPartnerId)
        {
            return Get<string>($"WebSiteDomain_${brandPartnerId}");
        }
        public static string ActOnListName = "l-7903";

        public static string ExecutionEnvironment => Get<string>("ExecutionEnvironment");
        public static string EnableSSL => Get<string>("EnableSSL");
        public static string TokenToReplace(int brandPartnerId)
        {
            return Get<string>($"TokenToReplace_${brandPartnerId}");
        }
        public static string GetEmailSenderName(int brandPartnerId)
        {
            return Get<string>($"EmailSenderName_${brandPartnerId}");
        }
        public static string NhspActonHeaderId => Get<string>("NHSPActonHeaderID");
        public static string NhspUnEngagedSuppressionListId => Get<string>("NHSPUnEngagedSuppressionListID");
        public static string NhspActonFooterId => Get<string>("NHSPActonFooterID");
        public static string ApiChildUserName => Get<string>("APIChildAccountUserName");
        public static string ApiChildPassword => Get<string>("APIChildAccountPassword");
        public static string ApiClientId => Get<string>("APIClientID");
        public static string ApiClientSecretKey => Get<string>("APIClientSecretKey");
    }
}
