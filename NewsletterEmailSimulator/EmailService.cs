﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using unirest_net.http;

namespace NewsletterEmailSimulator
{
    public class EmailService
    {
        public static object SendEmail(int marketId,int partnerId, string marketName,string userName, string partnerSiteUrl="")
        {
            StringBuilder step = new StringBuilder("just started");
            try
            {
                step.Append("step1");
                string emailTemplateUrl = Config.EmailTemplateUrl(88).Replace("[marketid]", marketId.ToString()).Replace("[weeklyMode]", "w").Replace("[partnerurl]", 88 == partnerId ? string.Empty : partnerSiteUrl);
                //string emailTemplateUrl = GenrateNewsletterUrl(partnerId, partnerSiteUrl, marketId, isRegistered);
                step.Append("step2");
                var htmlEmail = Nhs.Utility.Web.HTTP.GetHTML(Config.GetWebsiteDomain(88) + emailTemplateUrl);
                step.Append("step3");
                string htmlContent = FilterTemplateHtml(htmlEmail,userName);
                step.Append("step4");

                string subject = Config.GetEmailSubject(88).Replace("[MarketName]", marketName);
                step.Append("step5");
                subject = subject.Replace("[Month]", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Today.Month));
                subject = subject.Replace("[Day]", DateTime.Today.Day.ToString());
                step.Append("step6");
                Utility.LogMessageToFile("Email subject: " + subject);
                step.Append("step7");
                SendMessage(htmlContent, subject, 88);
                step.Append("Completed");
                return new {Message = step.ToString(), Status = true};
            }
            catch (Exception e)
            {
                step.Append("Error");
                Utility.LogExceptionToFile(e);
            }

            return new {Message = step.ToString(), Status = false};
        }

        private static string FilterTemplateHtml(string htmlEmail,string userName)
        {
            //foreach (var respToken in Config.TokenToReplace(88).Split(';'))
            //{
            //    var findAndReplce = respToken.Split('~');
            //    htmlEmail = htmlEmail.Replace(findAndReplce[0], findAndReplce[1]);
            //}

            htmlEmail = htmlEmail.Replace("$lookup(Name)$", $"{userName}");
            htmlEmail = htmlEmail.Replace("$lookup(GenerationDateYear)$",
                $"{DateTime.Today:MMMMM dd, yyyy}");
            htmlEmail = htmlEmail.Replace("$lookup(GenerationDate)$,", string.Empty);

            int startIdx = -1;
            int endIdx = -1;

            startIdx = htmlEmail.IndexOf("<!--$RemovePart1-->");
            endIdx = htmlEmail.IndexOf("<!--$EndOfRemovePart1-->") + 24;
            if (startIdx > 0)
                htmlEmail = htmlEmail.Remove(startIdx, endIdx - startIdx);

            startIdx = htmlEmail.IndexOf("<!--$RemovePart2-->");
            endIdx = htmlEmail.IndexOf("<!--$EndOfRemovePart2-->") + 24;
            if (startIdx > 0)
                htmlEmail = htmlEmail.Remove(startIdx, endIdx - startIdx);

            startIdx = htmlEmail.IndexOf("<!--$RemovePart3-->");
            endIdx = htmlEmail.IndexOf("<!--$EndOfRemovePart3-->") + 24;
            if (startIdx > 0)
                htmlEmail = htmlEmail.Remove(startIdx, endIdx - startIdx);
            return htmlEmail;
        }
        private static string SendMessage(string htmlBody, string subject, int brandPartnerId)
        {
            string url = $"{Config.BaseUrl}api/1/message/custom/send";
            AccessTokenResponse token = GetActOnAccessToken();
            StringBuilder bodyForm = new StringBuilder();
            bodyForm.AppendFormat("title={0}", HttpUtility.UrlEncode(subject));
            bodyForm.Append("&iscustom=Y");
            bodyForm.Append("&istextonly=N");
            bodyForm.Append("&issuppressduplicates=Y");
            bodyForm.Append("&suppressids=" + Config.NhspUnEngagedSuppressionListId);
            bodyForm.Append("&senderemail=" + HttpUtility.UrlEncode(Config.GetEmailFromAddress(brandPartnerId)));
            bodyForm.Append("&sendername=" + HttpUtility.UrlEncode(Config.GetEmailSenderName(brandPartnerId)));
            bodyForm.AppendFormat("&when={0}", Utility.DateTimeToUnixTimeStamp(DateTime.Now.AddMinutes(0)));
            bodyForm.AppendFormat("&subject={0}", HttpUtility.UrlEncode(subject));
            bodyForm.AppendFormat("&htmlbody={0}", HttpUtility.UrlEncode(htmlBody));
            bodyForm.AppendFormat("&headerid={0}", Config.NhspActonHeaderId);
            bodyForm.AppendFormat("&footerid={0}", Config.NhspActonFooterId);
            bodyForm.AppendFormat("&categoryid={0}", Config.EmailCategory(brandPartnerId));
            bodyForm.Append("&sendtoids=" + Config.ActOnListName);
            bodyForm.Append("&textbody=notextbody");
            Dictionary<string, object> httpHeaders = new Dictionary<string, object>
                        {
                            {"accept", "application/json"},
                            {"content-type", "application/x-www-form-urlencoded"},
                            {"Authorization", "Bearer " + token.Access_Token}
                        };

            var response = Unirest.post(url).headers(httpHeaders).body(bodyForm.ToString()).asString();

            Utility.LogMessageToFile("Response received from act-on for send email: " + response.Body);

            if (response.Body.ToLower().Contains("error"))
            {
                Utility.SendEmail("Error in act-on", response.Body);
            }
            return string.Empty;
        }
        private static AccessTokenResponse GetActOnAccessToken()
        {
            var jsonToken = Utility.GetAuthTokenFromFile();
            if (!string.IsNullOrEmpty(jsonToken))
            {
                Utility.LogMessageToFile("Token reused from child file: " + jsonToken);
                JavaScriptSerializer jsParser = new JavaScriptSerializer();
                AccessTokenResponse tokenFromFile = jsParser.Deserialize<AccessTokenResponse>(jsonToken);
                return tokenFromFile;
            }
            var url =
                $"{Config.BaseUrl}token?grant_type=password&{"username=" + Config.ApiChildUserName}&{"password=" + Config.ApiChildPassword}&{"client_id=" + Config.ApiClientId}&{"client_secret=" + Config.ApiClientSecretKey}";
            Utility.LogMessageToFile("Url for token " + url);

            Dictionary<string, object> httpHeaders = new Dictionary<string, object>
            {
                {"accept", "application/json"},
                {"content-type", "application/x-www-form-urlencoded"}
            };

            var response = Unirest.post(url).headers(httpHeaders).field("test", "test").asString();

            Utility.LogMessageToFile("Response recieved for token " + response.Body);

            JavaScriptSerializer js = new JavaScriptSerializer();
            AccessTokenResponse accessTokenResponse = js.Deserialize<AccessTokenResponse>(response.Body);
            Utility.TokenIssuedDateTime = DateTime.Now;
            Utility.LogMessageToFile("Access token parsed successfully.");
            Utility.SaveAuthToken(response.Body);
            return accessTokenResponse;
        }
    }
}
